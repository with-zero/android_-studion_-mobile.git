package com.example.mywork;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;
import java.util.Map;

public class Myadpter_Contract extends RecyclerView.Adapter<Myadpter_Contract.MyviewHolder> {


    private List<Map<String,Object>> data;
    private Context context;


    public Myadpter_Contract(Context context, List<Map<String,Object>> data) {
        this.context = context;
        this.data = data;
    }

    @NonNull
    @Override
    //就相当于指针，ViewHolder是抽象的，所以需要创建其子类返回
    public MyviewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        //视图压缩
        View view = LayoutInflater.from(context).inflate(R.layout.item_list_contact,parent,false);
        MyviewHolder holder = new MyviewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull MyviewHolder holder, int position) {
        holder.connection_text1.setText(data.get(position).get("key1").toString());
        holder.connection_text2.setText(data.get(position).get("key2").toString());

    }


    @Override
    public int getItemCount() {
        return data.size();
    }

    public class MyviewHolder extends RecyclerView.ViewHolder {
        TextView connection_text1,connection_text2;

        public MyviewHolder(@NonNull View itemView) {
            super(itemView);
            connection_text1 = itemView.findViewById(R.id.connection_text1);
            connection_text2 = itemView.findViewById(R.id.connection_text2);

        }
    }
}
