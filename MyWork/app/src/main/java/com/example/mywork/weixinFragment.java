package com.example.mywork;

import android.content.Context;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link weixinFragment#newInstance} factory method to
 * create an instance of this fragment.
 *
 */
public class weixinFragment extends Fragment {

    private RecyclerView recyclerView;
    private Myadpter_Weixin myadpter_weixin;
    private View view;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        view = inflater.inflate(R.layout.fragment_weixin,container,false);

        recyclerView = (RecyclerView) view.findViewById(R.id.RecycerView_weixin);


        //数据
        String [] people = {"柏拉图","阿基米德","柴科夫斯基","尤伯罗斯","洛必达","克莱姆","牛顿莱布尼茨"};
        String [] message = {"在线","离开","忙碌","举办奥运中","求极限中","矩阵求解中","合力积分中"};


        Context context = getActivity();

        List<Map<String,Object>> data = new ArrayList<>();
        for (int i = 0 ; i <people.length;i++)
        {
            Map<String,Object> itemdata = new HashMap<>();

            itemdata.put("key1",people[i]);
            itemdata.put("key2",message[i]);

            data.add(itemdata);
        }

        myadpter_weixin = new Myadpter_Weixin(context,data);

        LinearLayoutManager manager = new LinearLayoutManager(context);

        manager.setOrientation(RecyclerView.VERTICAL);

        recyclerView.setLayoutManager(manager);

        //分割线
        DividerItemDecoration mDivider = new DividerItemDecoration(context, DividerItemDecoration.VERTICAL);
        recyclerView.addItemDecoration(mDivider);

        recyclerView.setAdapter(myadpter_weixin);

        return view;
    }
}