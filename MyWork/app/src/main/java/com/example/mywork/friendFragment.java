package com.example.mywork;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.fragment.app.Fragment;
//import androidx.navigation.Navigation;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link friendFragment#} factory method to
 * create an instance of this fragment.
 *
 */
public class friendFragment extends Fragment {


    private LinearLayout tab01,tab02,tab03,tab04,tab05,tab06;
    private View view;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        Log.d("friend","22222222222");
        view = inflater.inflate(R.layout.fragment_fridend, container, false);

        tab01 = view.findViewById(R.id.friendtab01);
        tab02 = view.findViewById(R.id.friendtab02);
        tab03 = view.findViewById(R.id.friendtab03);
        tab04 = view.findViewById(R.id.friendtab04);
        tab05 = view.findViewById(R.id.friendtab05);
        tab06 = view.findViewById(R.id.friendtab06);

//        Intent intent1 = new Intent(getActivity(),BindMusicFragment.class);
//        Intent intent2 = new Intent(getActivity(),UnbindMusicFragment.class);


        tab05.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                FragmentManager fragmentManager = getActivity().getFragmentManager();
                Log.d("service1","onclick to start unbind......");
                MainActivity main = (MainActivity) getActivity();
                main.show(5);
//                fragmentManager.beginTransaction().addToBackStack(null).replace(R.id.friend_fragment,new BindMusicFragment()).commit();
//                startActivityForResult(intent1,1);
            }
        });

        tab06.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d("service1","onclic to start bind.....");
//                FragmentManager fragmentManager = getActivity().getFragmentManager();
                MainActivity main = (MainActivity) getActivity();
                main.show(6);
//                fragmentManager.beginTransaction().replace(R.id.friend_fragment,new UnbindMusicFragment()).addToBackStack(null).commit();
//                startActivityForResult(intent2,2);
            }
        });

        return view;


    }
}