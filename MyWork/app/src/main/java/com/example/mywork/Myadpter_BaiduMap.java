package com.example.mywork;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;
import java.util.Map;

public class Myadpter_BaiduMap extends RecyclerView.Adapter<Myadpter_BaiduMap.MyviewHolder> {


    private List<Map<String,Object>> data;
    private Context context;
    private MyItemClickListener myItemClickListener;
//    private LayoutInflater layoutInflater;



    public Myadpter_BaiduMap(Context context, List<Map<String,Object>> data) {
        this.context = context;
        this.data = data;

    }

    @NonNull
    @Override
    //就相当于指针，ViewHolder是抽象的，所以需要创建其子类返回
    public MyviewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        //视图压缩
        View view = LayoutInflater.from(context).inflate(R.layout.item_list_baidumap,parent,false);
        MyviewHolder holder = new MyviewHolder(view,myItemClickListener);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull MyviewHolder holder, int position) {
        holder.baidumap_text1.setText(data.get(position).get("key1").toString());
        holder.baidumap_text2.setText(data.get(position).get("key2").toString());
        holder.itemView.setTag(data.get(position));


    }

    @Override
    public int getItemCount() {
        return data.size();
    }


    public void setOnItemClickListener(MyItemClickListener myItemClickListener)
    {
        this.myItemClickListener = myItemClickListener;
    }


    public class MyviewHolder extends RecyclerView.ViewHolder {
        TextView baidumap_text1,baidumap_text2;
        private MyItemClickListener myItemClickListener;

        public MyviewHolder(@NonNull View itemView,MyItemClickListener myItemClickListener) {
            super(itemView);
            baidumap_text1 = itemView.findViewById(R.id.baidumap_text1);
            baidumap_text2 = itemView.findViewById(R.id.baidumap_text2);

            this.myItemClickListener = myItemClickListener;

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (myItemClickListener!=null) {
//                        if (getPosition()==0) {
//                            myItemClickListener.onItemClick1(view, 0);
//                        }
                        switch(getPosition())
                        {
                            case 0 :
                                myItemClickListener.onItemClick1(view, 0);break;
                            case 1 :
                                myItemClickListener.onItemClick2(view, 1);break;
                            case 2 :
                                myItemClickListener.onItemClick3(view, 2);break;
                            default: break;
                        }
                    }
                }
            });
        }
    }
}


interface MyItemClickListener{
    public void onItemClick1(View view,int postion);
    public void onItemClick2(View view,int postion);
    public void onItemClick3(View view,int postion);

}