package com.example.mywork;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.fragment.app.Fragment;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link settingFragment#newInstance} factory method to
 * create an instance of this fragment.
 *
 */
public class settingFragment extends Fragment {

    private LinearLayout tab01,tab02,tab03,tab04,tab05,tab06;
    private View view;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        view = inflater.inflate(R.layout.fragment_setting, container, false);

        tab01 = view.findViewById(R.id.settingtab01);
        tab02 = view.findViewById(R.id.settingtab02);
        tab03 = view.findViewById(R.id.settingtab03);
        tab04 = view.findViewById(R.id.settingtab04);
        tab05 = view.findViewById(R.id.settingtab05);
        tab06 = view.findViewById(R.id.settingtab06);

        Intent intent1 = new Intent(getActivity(),Baidu_Map.class);


        tab02.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Bundle bundle1 = new Bundle();
//                bundle1.putString("name","xr");
//                bundle1.putString("class","");
//                intent1.putExtras(bundle1);
////                startActivity(intent1);
                startActivityForResult(intent1,1);

            }
        });

        return view;
    }
}