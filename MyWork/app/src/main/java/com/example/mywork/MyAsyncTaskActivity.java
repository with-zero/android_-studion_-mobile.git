package com.example.mywork;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

public class MyAsyncTaskActivity extends AppCompatActivity {

    public static Button button;
    public static ProgressBar progressBar;
    public static ImageView imageView;
    public static TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        supportRequestWindowFeature(Window.FEATURE_NO_TITLE);//隐藏项目名
        setContentView(R.layout.activity_my_async_task);

        button = findViewById(R.id.button_myasynctask);
        progressBar = findViewById(R.id.progressBar_myasynctask);
        imageView = findViewById(R.id.imageView_myasynctask);
        textView = findViewById(R.id.textView_myasynctask);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new MyAsyncTask().execute();
            }
        });


    }
}