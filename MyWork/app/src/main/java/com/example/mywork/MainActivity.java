package com.example.mywork;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{


    //中间部分的四个Fragment
    private Fragment weixinFragment;
    private Fragment friendFragment;
    private Fragment contactFragment;
    private Fragment settingFragment;
    private Fragment bindmusicfragment;
    private Fragment unbindmusicfragment;

    //四个图片视图
    private ImageView img_weixin,img_contact,img_friend,img_setting,img_temp;

    //四个layout布局
    private LinearLayout tab01,tab02,tab03,tab04;

    private FragmentManager fm;

    private FragmentTransaction transaction;

    //动态授权
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        Log.d("aaa","??????");
        switch (requestCode){
            case 1:
                if(grantResults[0]!=PackageManager.PERMISSION_GRANTED){
                    Toast.makeText(this, "未授权，无法实现预定的功能！", Toast.LENGTH_SHORT).show();
                    finish();
                } else {
                    Toast.makeText(this, "请发一条短信验证！", Toast.LENGTH_SHORT).show();
                }
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        supportRequestWindowFeature(Window.FEATURE_NO_TITLE);//隐藏项目名
        setContentView(R.layout.activity_main);

        Log.d("activity","11111111111");

        //授权
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.RECEIVE_SMS) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{"android.permission.RECEIVE_SMS"}, 1);
        }

        weixinFragment = new weixinFragment();
        friendFragment = new friendFragment();
        contactFragment = new contactFragment();
        settingFragment = new settingFragment();
        bindmusicfragment = new BindMusicFragment();
        unbindmusicfragment = new UnbindMusicFragment();

        //通过id获取tab（对应LinearLayout）
        tab01 =findViewById(R.id.tab01);
        tab02 =findViewById(R.id.tab02);
        tab03 =findViewById(R.id.tab03);
        tab04 =findViewById(R.id.tab04);

        fm = getSupportFragmentManager();

        initalfragement();//加载并初始化显示Fragment页面
        initlImg();//加载初始化图片（通过id）,如果需要文字变化，也可以在这里初始化实现
        img_weixin.setSelected(true);//选择微信控件初始颜色

        tab01.setOnClickListener(this);
        tab02.setOnClickListener(this);
        tab03.setOnClickListener(this);
        tab04.setOnClickListener(this);

    }

    private void initlImg() {

        img_weixin = findViewById(R.id.imageView1);
        img_contact = findViewById(R.id.imageView2);
        img_friend = findViewById(R.id.imageView3);
        img_setting = findViewById(R.id.imageView4);
        img_temp = img_weixin;
    }

    private void initalfragement() {
        FragmentTransaction transaction = fm.beginTransaction();//开始做transaction
        transaction.add(R.id.content,weixinFragment);
        transaction.add(R.id.content,friendFragment);
        transaction.add(R.id.content,contactFragment);
        transaction.add(R.id.content,settingFragment);
        transaction.add(R.id.content,bindmusicfragment);
        transaction.add(R.id.content,unbindmusicfragment);
        Hide(transaction);//隐藏
        transaction.show(weixinFragment);
        transaction.commit();

    }

    @Override
    public void onClick(View v) {//点击后操作
        switch (v.getId()){
            case R.id.tab01: show(1); ImgColorChange(1);break;
            case R.id.tab02: show(2); ImgColorChange(2);break;
            case R.id.tab03: show(3); ImgColorChange(3);break;
            case R.id.tab04: show(4); ImgColorChange(4);break;
            default: break;
        }
    }

    public void show(int i) {//展示Fragement
        FragmentTransaction transaction = fm.beginTransaction();
        Hide(transaction);//隐藏
        switch (i)
        {
            case 1 : transaction.show(weixinFragment);break;
            case 2 : transaction.show(contactFragment);break;
            case 3 : transaction.show(friendFragment);break;
            case 4 : transaction.show(settingFragment);break;
            case 5 : transaction.show(unbindmusicfragment);break;
            case 6 : transaction.show(bindmusicfragment);break;
            default: break;
        }
        transaction.commit();//提交
    }

    private void ImgColorChange(int i)
    {
        img_temp.setSelected(false);
        switch(i){
            case 1: img_weixin.setSelected(true); img_temp = img_weixin; break;
            case 2: img_contact.setSelected(true); img_temp = img_contact; break;
            case 3: img_friend.setSelected(true); img_temp = img_friend; break;
            case 4: img_setting.setSelected(true); img_temp = img_setting ;break;
            default:break;
        }
    }


    private void Hide(FragmentTransaction transaction) {
        transaction.hide(weixinFragment);
        transaction.hide(friendFragment);
        transaction.hide(contactFragment);
        transaction.hide(settingFragment);
        transaction.hide(bindmusicfragment);
        transaction.hide(unbindmusicfragment);


    }

//    public void setTabSelection(int i) {
//        if (i==1){
//            transaction.show(bindmusicfragment);
//        }else if (i==2){
//            transaction.show(unbindmusicfragment);
//        }
//    }
}