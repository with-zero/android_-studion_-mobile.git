package com.example.mywork;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.View;
import android.view.Window;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Baidu_Map extends AppCompatActivity {

    private RecyclerView recyclerView;
    private Myadpter_BaiduMap myadpter_baiduMap;
    private View view;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        supportRequestWindowFeature(Window.FEATURE_NO_TITLE);//隐藏项目名
        setContentView(R.layout.activity_baidu_map);

        Intent intent2 = getIntent();

//        Bundle bundle=intent2.getExtras();
//        String Str1 = bundle.get("name").toString();
//        String Str2 = bundle.get("class").toString();

        intent2.putExtra("result","result:ok");
        setResult(1,intent2);


        //对recycleview进行配置
        recyclerView = (RecyclerView) this.findViewById(R.id.recyclerView_Baidu_Map);

        //数据
        String [] people = {"百度定位","MyAsyncTask","待开发..."};
        String [] message = {"","","后续即将上线"};

        Context context = this;

        List<Map<String,Object>> data = new ArrayList<>();
        for (int i = 0 ; i <people.length;i++)
        {
            Map<String,Object> itemdata = new HashMap<>();

            itemdata.put("key1",people[i]);
            itemdata.put("key2",message[i]);

            data.add(itemdata);
        }

        myadpter_baiduMap = new Myadpter_BaiduMap(context,data);

        LinearLayoutManager manager = new LinearLayoutManager(context);

        manager.setOrientation(RecyclerView.VERTICAL);

        recyclerView.setLayoutManager(manager);

        //分割线
        DividerItemDecoration mDivider = new DividerItemDecoration(context, DividerItemDecoration.VERTICAL);
        recyclerView.addItemDecoration(mDivider);


        recyclerView.setAdapter(myadpter_baiduMap);

        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        Intent intent1 = new Intent(this,BaiduMap_OpenUse.class);
        Intent intent3 = new Intent(this,MyAsyncTaskActivity.class);
        Intent intent4 = new Intent(this,TextActivity.class);

        myadpter_baiduMap.setOnItemClickListener(new MyItemClickListener() {
            @Override
            public void onItemClick1(View view, int postion) {

                startActivityForResult(intent1,2);
            }
            @Override
            public void onItemClick2(View view, int postion) {
                startActivityForResult(intent3,3);
            }
            @Override
            public void onItemClick3(View view, int postion) {
                startActivityForResult(intent4,4);
            }
        });



    }
}