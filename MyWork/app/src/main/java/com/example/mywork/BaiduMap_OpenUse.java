package com.example.mywork;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.baidu.location.BDLocation;
import com.baidu.location.BDLocationListener;
import com.baidu.location.LocationClient;
import com.baidu.location.LocationClientOption;
import com.baidu.mapapi.SDKInitializer;
import com.baidu.mapapi.map.BaiduMap;
import com.baidu.mapapi.map.MapStatusUpdate;
import com.baidu.mapapi.map.MapStatusUpdateFactory;
import com.baidu.mapapi.map.MapView;
import com.baidu.mapapi.model.LatLng;

public class BaiduMap_OpenUse extends AppCompatActivity {

    private LocationClient mLocationClient;
    private MapView mapView;
    private BaiduMap baiduMap;
    private boolean isFirstLocated = true;
    private TextView tv_Lat,tv_Lon,tv_address;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

//        setContentView(R.layout.activity_baidu_map_open_use);
//        setContentView(R.layout.activity_baidu_map);

        Log.d("map","55555555555555555555");
        Intent intent2 = getIntent();
        intent2.putExtra("result","result:ok");
        setResult(2,intent2);

        //如果没有定位权限，动态请求用户允许使用该权限
        if(ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)!= PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.ACCESS_FINE_LOCATION},1);
        }
        else {
            Log.d("map","66666666666666666");
            requestLocation();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case 1:
                if (grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(this, "没有定位权限", Toast.LENGTH_SHORT).show();
                    finish();
                } else {
                    requestLocation();
                }
        }
    }

    private void requestLocation() {
        //定位前初始化
        initLocation();
        //发起定位
        mLocationClient.start();
    }

    private void initLocation() {
        try {
            mLocationClient = new LocationClient(getApplicationContext());
            //注册监听
            mLocationClient.registerLocationListener(new MyLoctionListener());

            //初始化地图应用
//            SDKInitializer.setAgreePrivacy(this,true);
            SDKInitializer.initialize( getApplicationContext());

            setContentView(R.layout.activity_baidu_map_open_use);

            mapView = findViewById(R.id.BaiduMapView);
            baiduMap = mapView.getMap();
            tv_Lat = findViewById(R.id.tv_Lat);
            tv_Lon = findViewById(R.id.tv_Lon);
            tv_address = findViewById(R.id.tv_address);

            //定位客户端操作
            LocationClientOption option = new LocationClientOption();
            //设置扫描时间间隔
            option.setScanSpan(1000);
            //设置定位模式三选一
            option.setLocationMode(LocationClientOption.LocationMode.Hight_Accuracy);
//            option.setLocationMode(LocationClientOption.LocationMode.Battery_Saving);
//            option.setLocationMode(LocationClientOption.LocationMode.Device_Sensors);
            //设置需要地址信息
            option.setIsNeedAddress(true);
            //保存定位参数
            mLocationClient.setLocOption(option);


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private class MyLoctionListener implements BDLocationListener{

        @Override
        public void onReceiveLocation(BDLocation bdLocation) {
            tv_Lat.setText(bdLocation.getLatitude()+"");
            tv_Lon.setText(bdLocation.getLongitude()+"");
            tv_address.setText(bdLocation.getAddrStr());

            if (bdLocation.getLocType() == BDLocation.TypeGpsLocation|| bdLocation.getLocType() == BDLocation.TypeNetWorkLocation){
                navigateTo(bdLocation);
            }
        }
    }

    private void navigateTo(BDLocation bdLocation){
        if (isFirstLocated){
            LatLng latLng = new LatLng(bdLocation.getLatitude(),bdLocation.getLongitude());
            MapStatusUpdate update = MapStatusUpdateFactory.newLatLng(latLng);
            baiduMap.setMyLocationEnabled(true);
            baiduMap.animateMapStatus(update);
            isFirstLocated = false;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        mapView.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mapView.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }
}