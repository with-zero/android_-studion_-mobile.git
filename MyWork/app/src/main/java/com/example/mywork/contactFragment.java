package com.example.mywork;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link contactFragment#newInstance} factory method to
 * create an instance of this fragment.
 *
 */
public class contactFragment extends Fragment {


    private RecyclerView recyclerView;
    private Myadpter_Contract myadpter_contract;
    private View view;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.d("friend","cccccccccccccc");
        view = inflater.inflate(R.layout.fragment_contact, container, false);

        //对recycleview进行配置
        recyclerView = (RecyclerView) view.findViewById(R.id.recyclerView);


        //数据
        String [] people = {"柏拉图","阿基米德","柴科夫斯基","尤伯罗斯","洛必达","克莱姆","牛顿莱布尼茨"};
        String [] message = {"在线","离开","忙碌","举办奥运中","求极限中","矩阵求解中","合力积分中"};


        Context context = getActivity();

        List<Map<String,Object>> data = new ArrayList<>();
        for (int i = 0 ; i <people.length;i++)
        {
            Map<String,Object> itemdata = new HashMap<>();

            itemdata.put("key1",people[i]);
            itemdata.put("key2",message[i]);

            data.add(itemdata);
        }

        myadpter_contract = new Myadpter_Contract(context,data);

        LinearLayoutManager manager = new LinearLayoutManager(context);

        manager.setOrientation(RecyclerView.VERTICAL);

        recyclerView.setLayoutManager(manager);

        //分割线
        DividerItemDecoration mDivider = new DividerItemDecoration(context, DividerItemDecoration.VERTICAL);
        recyclerView.addItemDecoration(mDivider);


        recyclerView.setAdapter(myadpter_contract);

        return view;







        // Inflate the layout for this fragment
//        return inflater.inflate(R.layout.fragment_contact, container, false);
    }
}