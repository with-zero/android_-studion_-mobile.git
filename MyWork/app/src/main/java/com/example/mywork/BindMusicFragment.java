package com.example.mywork;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.os.IBinder;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link BindMusicFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class BindMusicFragment extends Fragment {

//    // TODO: Rename parameter arguments, choose names that match
//    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
//    private static final String ARG_PARAM1 = "param1";
//    private static final String ARG_PARAM2 = "param2";
//
//    // TODO: Rename and change types of parameters
//    private String mParam1;
//    private String mParam2;
//
//    public BindMusicFragment() {
//        // Required empty public constructor
//    }
//
//    /**
//     * Use this factory method to create a new instance of
//     * this fragment using the provided parameters.
//     *
//     * @param param1 Parameter 1.
//     * @param param2 Parameter 2.
//     * @return A new instance of fragment BindMusicFragment.
//     */
//    // TODO: Rename and change types and number of parameters
//    public static BindMusicFragment newInstance(String param1, String param2) {
//        BindMusicFragment fragment = new BindMusicFragment();
//        Bundle args = new Bundle();
//        args.putString(ARG_PARAM1, param1);
//        args.putString(ARG_PARAM2, param2);
//        fragment.setArguments(args);
//        return fragment;
//    }
//
//    @Override
//    public void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        if (getArguments() != null) {
//            mParam1 = getArguments().getString(ARG_PARAM1);
//            mParam2 = getArguments().getString(ARG_PARAM2);
//        }
//    }

    private View view;
    private LinearLayout tab01,tab02;
    Service1.Mybinder binder;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        Log.d("service1","bbbbbbbbbbbbbbb");
        view = inflater.inflate(R.layout.fragment_bind_music,container,false);



        Intent intentservices = new Intent(getActivity(),Service1.class);

        ServiceConnection connection= new ServiceConnection() {
            @Override
            public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
                binder = (Service1.Mybinder)iBinder;
                binder.myplay();
            }

            @Override
            public void onServiceDisconnected(ComponentName componentName) {
                binder=null;
            }
        };


        tab01 = view.findViewById(R.id.bindtab01);
        tab02 = view.findViewById(R.id.bindtab02);

        tab01.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d("service1","bind start music....");
                getActivity().bindService(intentservices,connection, Context.BIND_AUTO_CREATE);//绑定服务
            }
        });

        tab02.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Log.d("service1","bind stop music....");
                getActivity().unbindService(connection);
            }
        });


        return view;
//        return inflater.inflate(R.layout.fragment_bind_music, container, false);
    }
}